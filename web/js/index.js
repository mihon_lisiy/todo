/**
 * Created by Михаил on 12.02.2017.
 */
import { createStore } from 'redux'
import todoApp from './reducers'

let store = createStore(todoApp)

// ReactDOM.render(<TodoApp />, document.getElementById('root'));